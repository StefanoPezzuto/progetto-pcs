from unittest import TestCase

import src.empty_class as test


class TestPolygon(TestCase):
    def test_Rectangle(self):
        puntiRettangolo = []
        verticiRettangolo = []
        indici = [0, 1, 2, 3]
        coord = [1.0, 1.0, 5.0, 1.0, 5.0, 3.1, 1.0, 3.1]
        segmento = test.Segment(test.Point(2.0, 1.2), test.Point(4.0, 3.0))
        for i in range(0, len(coord), 2):
            puntiRettangolo.append(test.Point(coord[i], coord[i + 1]))

        for i in range(len(indici)):
            verticiRettangolo.append(test.Vertex(puntiRettangolo[i], indici[i]))
        Rettangolo = test.Polygon(verticiRettangolo)
        try:
            poligoniTagliati = Rettangolo.cut_polygon(puntiRettangolo, indici, segmento)
            nuoviPunti = puntiRettangolo
            nuoviVertici = verticiRettangolo
            nuoviPunti.append(test.Point(1.7777, 1.0))
            nuoviVertici.append(test.Vertex(test.Point(1.7777, 1.0), 4))
            nuoviPunti.append(test.Point(4.1111, 3.1))
            nuoviVertici.append(test.Vertex(test.Point(4.1111, 3.1), 5))
            nuoviPunti.append(test.Point(2.0, 1.2))
            nuoviVertici.append(test.Vertex(test.Point(2.0, 1.2), 6))
            nuoviPunti.append(test.Point(4.0, 3.0))
            nuoviVertici.append(test.Vertex(test.Point(4.0, 3.0), 7))
            for i in range(len(Rettangolo.PuntiPoligono)):
                self.assertEqual(Rettangolo.PuntiPoligono[i], nuoviPunti[i])
            nuoviPoligoni = []
            verticiConstr = [nuoviVertici[4], nuoviVertici[1], nuoviVertici[2], nuoviVertici[5], nuoviVertici[7],
                             nuoviVertici[6]]
            nuoviPoligoni.append(test.Polygon(verticiConstr))
            verticiConstr.clear()
            verticiConstr = [nuoviVertici[0], nuoviVertici[4], nuoviVertici[6], nuoviVertici[7], nuoviVertici[5],
                             nuoviVertici[3]]
            nuoviPoligoni.append(test.Polygon(verticiConstr))
            for i in range(len(poligoniTagliati)):
                self.assertEqual(poligoniTagliati[i], nuoviPoligoni)
        except Exception:
            self.fail()

    def test_Pentagon(self):
        puntiPentagono = []
        verticiPentagono = []
        indici = [0, 1, 2, 3, 4]
        coord = [2.5, 1.0, 4.0, 2.1, 3.4, 4.2, 1.6, 4.2, 1.0, 2.1]
        segmento = test.Segment(test.Point(1.4, 2.75), test.Point(3.6, 2.2))
        for i in range(0, len(coord), 2):
            puntiPentagono.append(test.Point(coord[i], coord[i+1]))
        for i in range(len(indici)):
            verticiPentagono.append(test.Vertex(puntiPentagono[i], indici[i]))
        Pentagono = test.Polygon(verticiPentagono)
        try:
            poligoniTagliati = Pentagono.cut_polygon(puntiPentagono, indici, segmento)
            nuoviPunti = puntiPentagono
            nuoviVertici = verticiPentagono
            nuoviPunti.append(test.Point(1.2, 2.8))
            nuoviVertici.append(test.Vertex(test.Point(1.2, 2.8), 5))
            nuoviPunti.append(segmento.A)
            nuoviVertici.append(test.Vertex(segmento.A, 6))
            nuoviPunti.append(segmento.B)
            nuoviVertici.append(test.Vertex(segmento.B, 7))
            for i in range(len(Pentagono.PuntiPoligono)):
                self.assertEqual(Pentagono.PuntiPoligono[i], nuoviPunti[i])
            nuoviPoligoni = []
            verticiCostr = [nuoviVertici[0], nuoviVertici[1], nuoviVertici[7], nuoviVertici[6], nuoviVertici[5],
                            nuoviVertici[4]]
            nuoviPoligoni.append(test.Polygon(verticiCostr))
            verticiCostr.clear()
            verticiCostr = [nuoviVertici[2], nuoviVertici[3], nuoviVertici[5], nuoviVertici[6], nuoviVertici[7],
                            nuoviVertici[7]]
            nuoviPoligoni.append(test.Polygon(verticiCostr))
            for i in range(len(poligoniTagliati)):
                self.assertEqual(poligoniTagliati[i], nuoviPoligoni)
        except Exception:
            self.fail()

    def test_ConcEsagon(self):
        PuntiConcavo = []
        indici = [0, 1, 2, 3, 4, 5]
        coord = [1.5, 1.0, 5.6, 1.5, 5.5, 4.8, 4.0, 6.2, 3.2, 4.2, 1.0, 4.0]
        segmento = test.Segment(test.Point(2.0, 3.7), test.Point(4.1, 5.9))
        for i in range(0, len(coord), 2):
            PuntiConcavo.append(test.Point(coord[i], coord[i+1]))

        VerticiConcavo = []
        for i in range(len(indici)):
            VerticiConcavo.append((test.Vertex(PuntiConcavo[i], indici[i])))

        ConcEsagon = test.Polygon(VerticiConcavo)
        try:
            PoligoniTagliati = ConcEsagon.cut_polygon(ConcEsagon.PuntiPoligono, indici, segmento)
            NuoviPunti = PuntiConcavo
            NuoviVertici = VerticiConcavo
            NuoviPunti.append(test.Point(4.2043, 6.0092))
            NuoviVertici.append(test.Vertex(test.Point(4.2043, 6.0092), 6))
            NuoviPunti.append(test.Point(3.7213, 5.5032))
            NuoviVertici.append(test.Vertex(test.Point(3.7213, 5.5032), 7))
            NuoviPunti.append(test.Point(2.4085, 4.1280))
            NuoviVertici.append(test.Vertex(test.Point(2.4085, 4.1280), 8))
            NuoviPunti.append(test.Point(1.1912, 2.8527))
            NuoviVertici.append(test.Vertex(test.Point(1.1912, 2.8527), 9))
            NuoviPunti.append(segmento.A)
            NuoviVertici.append(test.Vertex(segmento.A, 10))
            NuoviPunti.append(segmento.B)
            NuoviVertici.append(test.Vertex(segmento.B, 11))
            for i in range(len(ConcEsagon.PuntiPoligono)):
                self.assertEqual(ConcEsagon.PuntiPoligono[i], NuoviPunti[i])
            NuoviPoligoni = []
            VerticiCostr = [NuoviVertici[0], NuoviVertici[1], NuoviVertici[2], NuoviVertici[6], NuoviVertici[11],
                            NuoviVertici[7], NuoviVertici[4], NuoviVertici[8], NuoviVertici[10], NuoviVertici[9]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[3], NuoviVertici[7], NuoviVertici[11], NuoviVertici[6]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[5], NuoviVertici[9], NuoviVertici[10], NuoviVertici[8]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            for i in range(len(PoligoniTagliati)):
                self.assertEqual(PoligoniTagliati[i], NuoviPoligoni)
        except Exception:
            self.fail()

    def test_Decagono(self):
        PuntiDecagono = []
        Indici = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        coord = [2.0, -2.0, 0.0, -1.0, 3.0, 1.0, 0.0, 2.0, 3.0, 2.0, 3.0, 3.0, -1.0, 3.0, -3.0, 1.0, 0.0, 0.0, -3.0,
                 -2.0]
        segmento = test.Segment(test.Point(0.0, -3.0), test.Point(0.0, 4.0))
        for i in range(0, len(coord), 2):
            PuntiDecagono.append(test.Point(coord[i], coord[i+1]))
        VerticiDecagono = []
        for i in range(len(Indici)):
            VerticiDecagono.append((test.Vertex(PuntiDecagono[i], Indici[i])))
        Decagono = test.Polygon(VerticiDecagono)
        try:
            PoligoniTagliati = Decagono.cut_polygon(Decagono.PuntiPoligono, Indici, segmento)
            NuoviPunti = PuntiDecagono
            NuoviVertici = VerticiDecagono
            NuoviPunti.append(test.Point(0.0, 3.0))
            NuoviVertici.append(test.Vertex(test.Point(0.0, 3.0), 10))
            NuoviPunti.append(test.Point(0.0, -2.0))
            NuoviVertici.append(test.Vertex(test.Point(0.0, -2.0), 11))
            for i in range(len(Decagono.PuntiPoligono)):
                self.assertEqual(Decagono.PuntiPoligono[i], NuoviPunti[i])
            NuoviPoligoni = []
            VerticiCostr = [NuoviVertici[0], NuoviVertici[1], NuoviVertici[11]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[2], NuoviVertici[3], NuoviVertici[8], NuoviVertici[1]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[4], NuoviVertici[5], NuoviVertici[10], NuoviVertici[3]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[6], NuoviVertici[7], NuoviVertici[8], NuoviVertici[3], NuoviVertici[10]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[9], NuoviVertici[11], NuoviVertici[1], NuoviVertici[8]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            for i in range(len(PoligoniTagliati)):
                self.assertEqual(PoligoniTagliati[i], NuoviPoligoni)
        except Exception:
            self.fail()

    def test_DecagonoB(self):
        PuntiDecagonoB = []
        Indici = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        coord = [2.0, -2.0, 0.0, -1.0, 3.0, 1.0, 0.0, 2.0, 3.0, 2.0, 3.0, 3.0, -1.0, 3.0, -3.0, 1.0, 0.0, 0.0, -3.0,
                 -2.0]
        segmento = test.Segment(test.Point(-4.0, -4.0), test.Point(4.0, 4.0))
        for i in range(0, len(coord), 2):
            PuntiDecagonoB.append(test.Point(coord[i], coord[i+1]))
        VerticiDecagonoB = []
        for i in range(len(Indici)):
            VerticiDecagonoB.append((test.Vertex(PuntiDecagonoB[i], Indici[i])))
        DecagonoB = test.Polygon(VerticiDecagonoB)
        try:
            PoligoniTagliati = DecagonoB.cut_polygon(DecagonoB.PuntiPoligono, Indici, segmento)
            NuoviPunti = PuntiDecagonoB
            NuoviVertici = VerticiDecagonoB
            NuoviPunti.append(test.Point(1.5, 1.5))
            NuoviVertici.append(test.Vertex(test.Point(1.5, 1.5), 10))
            NuoviPunti.append(test.Point(2.0, 2.0))
            NuoviVertici.append(test.Vertex(test.Point(2.0, 2.0), 11))
            NuoviPunti.append(test.Point(-2.0, -2.0))
            NuoviVertici.append(test.Vertex(test.Point(-2.0, -2.0), 12))
            for i in range(len(DecagonoB.PuntiPoligono)):
                self.assertEqual(DecagonoB.PuntiPoligono[i], NuoviPunti[i])
            NuoviPoligoni = []
            VerticiCostr = [NuoviVertici[0], NuoviVertici[1], NuoviVertici[2], NuoviVertici[10], NuoviVertici[8],
                            NuoviVertici[12]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[4], NuoviVertici[5], NuoviVertici[11]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[3], NuoviVertici[11], NuoviVertici[5], NuoviVertici[6], NuoviVertici[7],
                            NuoviVertici[8], NuoviVertici[10]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[9], NuoviVertici[12], NuoviVertici[8]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            for i in range(len(PoligoniTagliati)):
                self.assertEqual(PoligoniTagliati[i], NuoviPoligoni)
        except Exception:
            self.fail()

    def test_Rombo(self):
        PuntiRombo = []
        Indici = [0, 1, 2, 3]
        coord = [2.0, 0.0, 0.0, 4.0, -2.0, 0.0, 0.0, -4.0]
        segmento = test.Segment(test.Point(0.0, 0.0), test.Point(-10.0, 10.0))
        for i in range(0, len(coord), 2):
            PuntiRombo.append(test.Point(coord[i], coord[i+1]))
        VerticiRombo = []
        for i in range(len(Indici)):
            VerticiRombo.append((test.Vertex(PuntiRombo[i], Indici[i])))
        Rombo = test.Polygon(VerticiRombo)
        try:
            PoligoniTagliati = Rombo.cut_polygon(Rombo.PuntiPoligono, Indici, segmento)
            NuoviPunti = PuntiRombo
            NuoviVertici = VerticiRombo
            NuoviPunti.append(test.Point(-1.3333, 1.3333))
            NuoviVertici.append(test.Vertex(test.Point(-1.3333, 1.3333), 4))
            NuoviPunti.append(test.Point(1.3333, -1.3333))
            NuoviVertici.append(test.Vertex(test.Point(1.3333, -1.3333), 5))
            NuoviPunti.append(test.Point(0.0, 0.0))
            NuoviVertici.append(test.Vertex(test.Point(0.0, 0.0), 6))
            for i in range(len(Rombo.PuntiPoligono)):
                self.assertEqual(Rombo.PuntiPoligono[i], NuoviPunti[i])
            NuoviPoligoni = []
            VerticiCostr = [NuoviVertici[0], NuoviVertici[1], NuoviVertici[4], NuoviVertici[6], NuoviVertici[5]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            VerticiCostr = [NuoviVertici[2], NuoviVertici[3], NuoviVertici[5], NuoviVertici[6], NuoviVertici[4]]
            NuoviPoligoni.append(test.Polygon(VerticiCostr))
            VerticiCostr.clear()
            for i in range(len(PoligoniTagliati)):
                self.assertEqual(PoligoniTagliati[i], NuoviPoligoni)
        except Exception:
            self.fail()
