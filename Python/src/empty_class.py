import numpy as np


class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y

    """costruttore copia classe Point"""
    def __copy__(self):
        return Point(self.X, self.Y)

    """Definizione operatore di confronto"""
    def __eq__(self, other):
        toll = 1.0e-04
        if (other.X - toll <= self.X <= other.X + toll) and (other.Y - toll <= self.Y <= other.Y + toll):
            return True
        else:
            return False


class Vertex:
    def __init__(self, p: Point, numerovertice: int):
        self.P = p
        self.NumeroVertice = numerovertice

    """Definizione operatore di confronto, usando quello definito nel punto"""
    def __eq__(self, other):
        if (other.P == self.P) and (other.NumeroVertice == self.NumeroVertice):
            return True
        else:
            return False


class Segment:
    def __init__(self, a: Point, b: Point):
        self.A = a
        self.B = b


class PuntiRettaIntersez:
    def __init__(self, p: Point, numvertice: int, latointersez: int, coordpar: float):
        self.P = p
        self.NumVertice = numvertice
        self.LatoIntersez = latointersez
        self.CoordPar = coordpar


class Polygon:
    def __init__(self, verticipoligono: []):
        self.SegmentiPoligono = []
        self.PuntiPoligono = []
        self.VerticiPoligono = verticipoligono

        for i in range(len(verticipoligono)):
            self.PuntiPoligono.append(self.VerticiPoligono[i].P)
        for i in range(len(verticipoligono)):
            if i != len(verticipoligono) - 1:
                self.SegmentiPoligono.append(Segment(self.PuntiPoligono[i], self.PuntiPoligono[i + 1]))
            else:
                self.SegmentiPoligono.append(Segment(self.PuntiPoligono[i], self.PuntiPoligono[0]))

    """Definizione operatore di confronto Poligoni"""
    def __eq__(self, other):
        flag = 0
        for i in range(len(self.VerticiPoligono)):
            if other.VerticiPoligono[i] != self.VerticiPoligono[i]:
                flag = 1
                break
        if flag == 0:
            return True
        else:
            return False

    def cut_polygon(self, points: [], vertices: [], segment: Segment):
        vertici = []

        """Creazione del vettore di vertici del poligono tramite i punti e gli interi passati in input
        alla funzione, ASSUNZIONE: la numerazione è data in senso antiorario"""
        for i in range(len(points)):
            vertici.append(Vertex(points[i], vertices[i]))
        poligono = Polygon(vertici)
        newpoint = Point(0, 0)
        puntisegmento = []

        """Identifichiamo un punto esterno al poligono, che ci servirà
        per verificare SUCCESSIVAMENTE se gli estremi A e B del segmento sono dentro al poligono oppure no"""
        xmin = self.PuntiPoligono[0].X
        ymin = self.PuntiPoligono[0].Y
        for i in range(len(self.PuntiPoligono)):
            if self.PuntiPoligono[i].X < xmin:
                xmin = self.PuntiPoligono[i].X
            if self.PuntiPoligono[i].Y < ymin:
                ymin = self.PuntiPoligono[i].Y
        puntoest = Point(xmin - 1, ymin - 1)

        """Andiamo a creare il sistema del tipo Ax = b per le intersezioni"""
        inva = np.zeros((2, 2))  # matrice inversa di A (divideremo poi per il determinante)
        """Definiamo le entrate "costanti" relative al segmento tagliante"""
        inva[0, 1] = -(segment.B.Y - segment.A.Y)
        inva[1, 1] = (segment.B.X - segment.A.X)
        toll = 1.0e-04  # setting della tolleranza
        """Ciclo for sui segmenti del poligono, verifichiamo per ogni segmento se c'è o meno intersezione"""
        for i in range(len(poligono.SegmentiPoligono)):
            """Definiamo le restanti entrate di inva"""
            inva[1, 0] = -(self.SegmentiPoligono[i].A.X - self.SegmentiPoligono[i].B.X)
            inva[0, 0] = (self.SegmentiPoligono[i].A.Y - self.SegmentiPoligono[i].B.Y)
            """termine noto sistema: differenza coordinate del punto iniziale di ciascun segmento"""
            b = np.array([[self.SegmentiPoligono[i].A.X - segment.A.X], [self.SegmentiPoligono[i].A.Y - segment.A.Y]])
            parametriccoord = [0, 0]  # coordinate parametriche sistema (incognite)
            """Soluzione sistema"""
            parametriccoord[0] = np.dot(inva.T[0], b)
            parametriccoord[1] = np.dot(inva.T[1], b)
            parametriccoord[0] = parametriccoord[0] / np.linalg.det(inva)
            parametriccoord[1] = parametriccoord[1] / np.linalg.det(inva)

            """Definizione di dove si trovi l'intersezione RISPETTO AL SEGMENTO DEL POLIGONO"""

            #  s(1) = 1, intersezione di tipo end
            if 1.0 - toll < parametriccoord[1] < 1.0 + toll:
                newpoint.x = self.SegmentiPoligono[i].A.X + (self.SegmentiPoligono[i].B.X -
                                                             self.SegmentiPoligono[i].A.X) * parametriccoord[1]
                newpoint.y = self.SegmentiPoligono[i].A.Y + (self.SegmentiPoligono[i].B.Y -
                                                             self.SegmentiPoligono[i].A.Y) * parametriccoord[1]
                """Poichè intersezione di tipo end, attribuiamo come numero vertice lo stesso del poligono"""
                numeroNuovoVertice = i + 1
                """Ci salviamo in un vettore tutti i punti del segmento tagliante con le informazioni di dove interseca
                e di dove si trova sulla retta tagliante"""
                puntisegmento.append(PuntiRettaIntersez(newpoint, numeroNuovoVertice, i, parametriccoord[0]))

            #  0<s(1)<1 , intersezione di tipo inner"""
            elif toll < parametriccoord[1] < 1.0 - toll:
                newpoint.X = self.SegmentiPoligono[i].A.X + (self.SegmentiPoligono[i].B.X -
                                                             self.SegmentiPoligono[i].A.X) * parametriccoord[1]
                newpoint.Y = self.SegmentiPoligono[i].A.Y + (self.SegmentiPoligono[i].B.Y -
                                                             self.SegmentiPoligono[i].A.Y) * parametriccoord[1]
                """Come da requirements andiamo a modificare il vettore di punti del poligono"""
                self.PuntiPoligono.append(Point.__copy__(newpoint))
                """Impostiamo come numero del vertice il numero successivo all'ultimo vertice del poligono"""
                numeroNuovoVertice = len(self.PuntiPoligono) - 1
                """Ci salviamo in un vettore tutti i punti del segmento con le informazioni di dove interseca e di dove 
                si trova sulla retta"""
                puntisegmento.append(PuntiRettaIntersez(newpoint, numeroNuovoVertice, i, parametriccoord[0]))

        """Vedo se A e B sono esterni o interni al poligono, nel primo caso non li aggiungiamo al vettore di punti 
        nel secondo sì.
        Algoritmo Ray-Casting
        
        Andiamo a creare il sistema del tipo A1x = c per l'estremo A
        e B1x=c per l'estremo B e contiamo le intersezioni"""
        invA1 = np.zeros((2, 2))
        invB1 = np.zeros((2, 2))
        invA1[1, 0] = -(segment.A.Y - puntoest.Y)
        invA1[1, 1] = (segment.A.X - puntoest.X)
        invB1[1, 0] = -(segment.B.Y - puntoest.Y)
        invB1[1, 1] = (segment.B.X - puntoest.X)
        IntersezA = 0
        IntersezB = 0

        for i in range(len(self.SegmentiPoligono)):
            invA1[0, 1] = -(self.SegmentiPoligono[i].A.X - self.SegmentiPoligono[i].B.X)
            invA1[0, 0] = self.SegmentiPoligono[i].A.Y - self.SegmentiPoligono[i].B.Y
            invB1[0, 1] = -(self.SegmentiPoligono[i].A.X - self.SegmentiPoligono[i].B.X)
            invB1[0, 0] = self.SegmentiPoligono[i].A.Y - self.SegmentiPoligono[i].B.Y
            c = np.array([[self.SegmentiPoligono[i].A.X - puntoest.X], [self.SegmentiPoligono[i].A.Y - puntoest.Y]])
            parametricCoordA1 = np.dot(invA1, c)
            parametricCoordB1 = np.dot(invB1, c)
            parametricCoordA1 /= np.linalg.det(invA1)
            parametricCoordB1 /= np.linalg.det(invB1)
            """Valutiamo se c'è e dove si trova l'intersezione. Se è di tipo Inner o End RISPETTO AD ENTRAMBI i segmenti
            Allora aumentiamo di 1 il counter"""
            if 0.0 + toll < parametricCoordA1[1] <= 1.0 + toll and 0.0 + toll < parametricCoordA1[0] <= 1.0 + toll:
                IntersezA += 1
            if 0.0 + toll < parametricCoordB1[1] <= 1.0 + toll and 0.0 + toll < parametricCoordB1[0] <= 1.0 + toll:
                IntersezB += 1
        flagA = 0
        flagB = 0
        if IntersezA % 2 != 0:
            for intersezione in puntisegmento:
                if segment.A == intersezione.P:
                    flagA = 1
                    break

            """Se il punto è interno e non coincide con nessun punto di intersezione già trovato
            allora lo aggiungiamo in entrambi i vettori"""
            if flagA != 1:
                self.PuntiPoligono.append(segment.A)
                """Per gli estremi A e B, poniamo lato intersezione uguale ad un numero che non è possibile rappresenti
                un'intersezione"""
                puntisegmento.append(PuntiRettaIntersez(segment.A, len(self.PuntiPoligono) - 1,
                                                        len(self.SegmentiPoligono) + 1, 0.00))

        """Ripetiamo lo stesso per l'estremo B"""
        if IntersezB % 2 != 0:
            for intersezione in puntisegmento:
                if segment.B == intersezione.P:
                    flagB = 1
                    break

            if flagB != 1:
                self.PuntiPoligono.append(segment.B)
                puntisegmento.append(PuntiRettaIntersez(segment.B, len(self.PuntiPoligono) - 1,
                                                        len(self.SegmentiPoligono) + 1, 1.00))

        """ordinamento crescente vettore puntisegmento"""
        for i in range(len(puntisegmento)):
            for j in range(i + 1, len(puntisegmento), 1):
                if puntisegmento[i].CoordPar > puntisegmento[j].CoordPar:
                    puntisegmento.append(puntisegmento[i])
                    puntisegmento[i] = puntisegmento[j]
                    puntisegmento[j] = puntisegmento[len(puntisegmento) - 1]
                    puntisegmento.pop()

        """Suddivisione dei punti originari del poligono in base alla loro posizione rispetto al tagliante"""
        verticiDx = []
        verticiSx = []

        """coordinate del vettore V = AB"""
        Vx = segment.B.X - segment.A.X
        Vy = segment.B.Y - segment.A.Y
        flag = 0  # 1 SE i-1 E' A SX, 0 SE i-1 E' A DX, DA DEFINIRE IN CASO DI PRIMO PUNTO

        """Ciclo sui punti originari del Poligono"""
        for i in range(len(self.VerticiPoligono)):
            """z = VxWy-WxVy, ove W=Punto[i]-A"""
            z = (Vx * (self.VerticiPoligono[i].P.Y - segment.A.Y)) - ((self.VerticiPoligono[i].P.X - segment.A.X) * Vy)

            if z > toll:  # se z>0 il punto è a sx del tagliante
                verticiSx.append(self.VerticiPoligono[i])
                flag = 1

            elif z < -toll:  # se z<0 il punto è a dx del tagliante
                verticiDx.append(self.VerticiPoligono[i])
                flag = 0

            elif -toll < z < toll:  # se z=0 guardiamo il flag e classifichiamo il punto come quello precedente
                if flag == 1:
                    verticiSx.append(self.VerticiPoligono[i])
                elif flag == 0:
                    verticiDx.append(self.VerticiPoligono[i])

        """INIZIO ALGORITMO DI TAGLIO"""
        nuoviPoligoni = []
        verticiNuovoPoligono = []  # Vettore d'appoggio per creazione dei sottopoligoni

        PartenzaPoligono = -1  # Indica l'inizio del sottopoligono
        newflag = 0            # Si attiva se il punto d'intersezione è un vertice di sx
        positionflag = 0       # Si attiva se il punto del segmento è un punto d'intersezione
        EndFlag = 0            # Si attiva se vogliamo uscire dal ciclo di k e quindi di j

        for i in range(len(verticiDx)):
            """aggiungiamo vertici dx se NON coincidono con un punto in PuntiSegmento"""
            IntersectionFlag = 0  # Si attiva se Vertice considerato coincide con un punto d'intersezione
            for e in range(len(puntisegmento)):
                if verticiDx[i].NumeroVertice == puntisegmento[e].NumVertice:
                    IntersectionFlag = 1
                    break

            if IntersectionFlag != 1:
                verticiNuovoPoligono.append(verticiDx[i])
                if PartenzaPoligono == -1:
                    if verticiDx[i].NumeroVertice != 0:
                        PartenzaPoligono = verticiDx[i].NumeroVertice - 1
                    else:
                        PartenzaPoligono = len(self.SegmentiPoligono) - 1

            for j in range(len(puntisegmento)):
                """Verifichiamo se c'è intersezione sul segmento che parte dal vertice considerato"""
                if verticiDx[i].NumeroVertice == puntisegmento[j].LatoIntersez:
                    """Se c'è intersezione aggiungiamo il punto d'intersezione al sottopoligono"""
                    verticiNuovoPoligono.append(Vertex(puntisegmento[j].P, puntisegmento[j].NumVertice))

                    """Scorriamo all'indietro lungo la retta del segmento tagliante (POICHE' SIAMO SUI VERTICI A DX 
                    DEL SEGMENTO)"""
                    for k in range(j - 1, 0, -1):
                        """AGGIUNGO IL PUNTO"""
                        verticiNuovoPoligono.append(Vertex(puntisegmento[k].P, puntisegmento[k].NumVertice))
                        """Verifico se il punto che stiamo aggiungendo è un vertice di sx"""
                        for u in range(len(verticiSx)):
                            if puntisegmento[k].NumVertice == verticiSx[u].NumeroVertice:
                                newflag = 1
                        """Se sono su 'PartenzaPoligono' creo il nuovo poligono
                        Controlliamo anche il numero del vertice in caso di intersezione di tipo End"""
                        if puntisegmento[k].LatoIntersez == PartenzaPoligono:
                            nuoviPoligoni.append(Polygon(verticiNuovoPoligono))
                            verticiNuovoPoligono.clear()
                            PartenzaPoligono = -1
                            EndFlag = 1
                            break  # USCIAMO DAL CICLO DI K

                        """ Verifico se il punto che sto aggiungendo è d'intersezione e non è un vertice di sx
                        in tal caso allora finisco di scorrere sulla retta del tagliante e torno a scorrere
                        sul poligono"""
                        if puntisegmento[k].LatoIntersez != len(self.SegmentiPoligono) + 1 and newflag == 0:
                            positionflag = 1
                            EndFlag = 1
                            break  # USCIAMO DAL CICLO DI K

                        """Se k=0 siamo sull'ultimo punto di PuntiSegmento, quindi torniamo a scorrere sul poligono"""
                        if k == 0:
                            positionflag = 1
                            EndFlag = 1  # CHIUSURA CICLO K

                    if EndFlag == 1:
                        EndFlag = 0
                        break  # USCIAMO DAL CICLO DI J

                else:
                    """Creiamo comunque il poligono quando siamo sull'ultimo vertice di dx e non ci sono più 
                    intersezioni"""
                    if positionflag == 1 and i == len(verticiDx) - 1 and j == len(verticiDx) - 1:
                        nuoviPoligoni.append(Polygon(verticiNuovoPoligono))
                        verticiNuovoPoligono.clear()
                        PartenzaPoligono = -1
                        break

        PartenzaPoligono = -1
        newflag = 0
        positionflag = 0
        EndFlag = 0
        for i in range(len(verticiSx)):
            """AGGIUNGO VERTICI SX SE NON E' COINCIDENTE CON UN PUNTO DI INTERSEZIONE"""
            IntersectionFlag = 0
            for e in range(len(puntisegmento)):
                if verticiSx[i].NumeroVertice == puntisegmento[e].NumVertice:
                    IntersectionFlag = 1
                    break

            if IntersectionFlag != 1:
                verticiNuovoPoligono.append(verticiSx[i])
                if PartenzaPoligono == -1:
                    if verticiSx[i].NumeroVertice != 0:
                        PartenzaPoligono = verticiSx[i].NumeroVertice - 1
                    else:
                        PartenzaPoligono = len(self.SegmentiPoligono) - 1

                for j in range(len(puntisegmento)):
                    if verticiSx[i].NumeroVertice == puntisegmento[j].LatoIntersez:
                        verticiNuovoPoligono.append(Vertex(puntisegmento[j].P, puntisegmento[j].NumVertice))

                        """Scorro in avanti sulla retta del tagliante (POICHE' SIAMO SUI VERTICI A SX DEL 
                        SEGMENTO TAGLIANTE)"""
                        for k in range(j + 1, len(puntisegmento), +1):
                            verticiNuovoPoligono.append(Vertex(puntisegmento[k].P, puntisegmento[k].NumVertice))

                            """Verifico se il punto che sto aggiungendo è un vertice di dx"""
                            for u in range(len(verticiDx)):
                                if puntisegmento[k].NumVertice == verticiDx[u].NumeroVertice:
                                    newflag = 1

                            if puntisegmento[k].LatoIntersez == PartenzaPoligono or puntisegmento[k].NumVertice == \
                                    PartenzaPoligono:
                                nuoviPoligoni.append(Polygon(verticiNuovoPoligono))
                                verticiNuovoPoligono.clear()
                                PartenzaPoligono = -1
                                EndFlag = 1
                                break  # USCIAMO DAL CICLO DI K

                            """VERIFICO SE PUNTO CHE STO AGGIUNGENDO E' D'INTERSEZIONE E NON E' UN VERTICE DI DX
                            IN TAL CASO ALLORA FINISCO DI SCORRERE SULLA RETTA DEL TAGLIANTE E TORNO A SCORRERE
                            SUL POLIGONO"""
                            if puntisegmento[k].LatoIntersez != len(self.SegmentiPoligono) + 1 and newflag == 0:
                                positionflag = 1
                                EndFlag = 1
                                break  # USCIAMO DAL CICLO DI K

                        if EndFlag == 1:
                            EndFlag = 0
                            break  # USCIAMO DAL CICLO DI J

                    else:
                        """CREIAMO COMUNQUE IL POLIGONO QUANDO SIAMO SULL'ULTIMO VERTICE DI SX E NON CI SONO PIU' 
                        INTERSEZIONI"""
                        if positionflag == 1 and i == len(verticiSx) - 1 and j == len(verticiSx) - 1:
                            nuoviPoligoni.append(Polygon(verticiNuovoPoligono))
                            verticiNuovoPoligono.clear()
                            break

        return nuoviPoligoni
