#include "EmptyClass.hpp"

namespace ProjectNamespace {

// Costruttore Punto
Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

// Costruttore copia Punto
Point::Point(const Point &point)
{
    X = point.X;
    Y = point.Y;
}

// Definizione dell'operatore di assegnazione
Point Point::operator = (const Point &point)
{
    return Point(point);
}

// Definizione dell'operatore di confronto
bool Point::operator ==(const Point &point) const
{
    //set tolleranza
    double toll = 1.0e-04;

    if((X<=point.X+toll) && (X>=point.X-toll) && (Y<=point.Y+toll) && (Y>=point.Y-toll))
        return true;
    else
        return false;
}

// Costruttore Vertice
Vertex::Vertex(const Point &p, const unsigned int &n): P(p), numeroVertice(n){}

// Costruttore copia Vertice
Vertex::Vertex(const Vertex &vertice): P(vertice.P), numeroVertice(vertice.numeroVertice){}

// Definizione operatore di confronto, usando quello definito nel punto
bool Vertex::operator ==(const Vertex &vertex) const
{
    if((P==vertex.P) && (numeroVertice==vertex.numeroVertice))
        return true;
    else
        return false;
}

// Costruttore Segmento
Segment::Segment(const Point &p1, const Point &p2): A(p1), B(p2){}

// Costruttore copia Segmento
Segment::Segment(const Segment &segment): A(segment.A), B(segment.B) {}

// Costruttore PuntiRettaIntersezione
PuntiRettaIntersez::PuntiRettaIntersez(const Point Punto, const unsigned int n,
                                       const unsigned int l, const float c): p(Punto)
{
    numVertice = n;
    latoIntersez = l;
    CoordPar = c;
}

// Costruttore copia PuntiRettaIntersezione
PuntiRettaIntersez::PuntiRettaIntersez(const PuntiRettaIntersez &puntorettaintersez): p(puntorettaintersez.p)
{
    numVertice = puntorettaintersez.numVertice;
    latoIntersez = puntorettaintersez.latoIntersez;
    CoordPar = puntorettaintersez.CoordPar;

}

// Definizione operatore di assegnazione
PuntiRettaIntersez PuntiRettaIntersez::operator =(PuntiRettaIntersez &puntorettaintersez)
{
    p.X = puntorettaintersez.p.X;
    p.Y = puntorettaintersez.p.Y;
    numVertice = puntorettaintersez.numVertice;
    latoIntersez = puntorettaintersez.latoIntersez;
    CoordPar = puntorettaintersez.CoordPar;
    return(PuntiRettaIntersez(p , numVertice, latoIntersez, CoordPar));
}

// Costruttore Poligono
Polygon::Polygon(vector<Vertex> Vertici)
{    
    VerticiPoligono = Vertici;

    for(unsigned int i=0; i<Vertici.size(); i++){
         PuntiPoligono.push_back(Vertici[i].P);
    }

    for(unsigned int i=0; i<Vertici.size(); i++){
        if (i != Vertici.size()-1)
            SegmentiPoligono.push_back(Segment(PuntiPoligono[i], PuntiPoligono[i+1]));
        else if (i== Vertici.size()-1 )
            SegmentiPoligono.push_back(Segment(PuntiPoligono[i], PuntiPoligono[0]));
    }

}

// Costruttore copia Poligono
Polygon::Polygon(const Polygon &polygon){
    VerticiPoligono = polygon.VerticiPoligono;
    PuntiPoligono = polygon.PuntiPoligono;
    SegmentiPoligono = polygon.SegmentiPoligono;
}

// Definizione operatore di confronto Poligoni
bool Polygon::operator ==(const Polygon &polygon) const
{
    if(VerticiPoligono==polygon.VerticiPoligono)
        return true;
    else
        return false;
}

// Inizio funzione taglio poligono
vector<Polygon> Polygon::CutPolygon(vector<Point> &PuntiPoligono, const vector<unsigned int> Vertices,
                                    const Segment Segment)
{
   // Creazione del vettore di vertici del poligono tramite i punti e gli interi passati in input
   // alla funzione, ASSUNZIONE: la numerazione è data in senso antiorario
   vector<Vertex> VerticiPoligono;
   for(unsigned int i=0; i<PuntiPoligono.size(); i++){
       VerticiPoligono.push_back(Vertex(PuntiPoligono[i], Vertices[i]));
   }
   Polygon Poligono = Polygon(VerticiPoligono);

   // Identifichiamo un punto esterno al poligono, che ci servirà
   // per verificare SUCCESSIVAMENTE se gli estremi A e B del segmento sono dentro al poligono oppure no
   double Xmin = PuntiPoligono[0].X;
   double Ymin = PuntiPoligono[0].Y;
   for(unsigned int i=1; i<PuntiPoligono.size(); i++){
       if(PuntiPoligono[i].X < Xmin)
           Xmin = PuntiPoligono[i].X;
       if(PuntiPoligono[i].Y < Ymin)
           Ymin = PuntiPoligono[i].Y;
   }
   Point PuntoEst = Point(Xmin-1 , Ymin-1);

   // Creazione vettore che conterrà punti di intersezione ed eventualmente A e B
   vector<PuntiRettaIntersez> PuntiSegmento;

   // Creazione punto ausiliario newpoint (variabile locale)
   Point newpoint = Point(0,0);
   //andiamo a creare il sistema del tipo Ax = b per le intersezioni
   Matrix2d inva; //matrice inversa di A (divideremo poi per il determinante)
   // Definiamo le entrate "costanti" relative al segmento tagliante
   inva(1,0) = -(Segment.B.Y - Segment.A.Y);
   inva(1,1) = (Segment.B.X - Segment.A.X);
   Vector2d parametriccoord; //coordinate parametriche sistema (incognite)
   Vector2d b; //termine noto sistema
   double toll = 1.0e-10; //setting della tolleranza

   // Ciclo for sui segmenti del poligono, verifichiamo per ogni segmento se c'è o meno intersezione
   for(unsigned int i=0; i<Poligono.SegmentiPoligono.size(); i++){
       // Definiamo le restanti entrate di inva
       inva(0,1)=-(SegmentiPoligono[i].A.X - SegmentiPoligono[i].B.X);
       inva(0,0)= SegmentiPoligono[i].A.Y - SegmentiPoligono[i].B.Y;
       // b = differenza coordinate del punto iniziale di cascun segmento
       b(0) = (SegmentiPoligono[i].A.X - Segment.A.X);
       b(1) = (SegmentiPoligono[i].A.Y - Segment.A.Y);
       // Soluzione sistema
       parametriccoord = inva * b;
       // Dividiamo ora per il determinante
       parametriccoord /= inva.determinant();

       // Definizione di dove si trovi l'intersezione RISPETTO AL SEGMENTO DEL POLIGONO

       // s(1) = 1, intersezione di tipo end
       if ((parametriccoord(1) > 1.0 - toll) && (parametriccoord(1) < 1.0 + toll))
       {
         newpoint.X = SegmentiPoligono[i].A.X + (SegmentiPoligono[i].B.X - SegmentiPoligono[i].A.X)*parametriccoord(1);
         newpoint.Y = SegmentiPoligono[i].A.Y + (SegmentiPoligono[i].B.Y - SegmentiPoligono[i].A.Y)*parametriccoord(1);
         // Poichè intersezione di tipo end, attribuiamo come numero vertice lo stesso del poligono
         unsigned int numeronuovovertice = i+1;
         // Ci salviamo in un vettore tutti i punti del segmento tagliante con le informazioni di dove interseca
         // e di dove si trova sulla retta tagliante
         PuntiSegmento.push_back(PuntiRettaIntersez(newpoint, numeronuovovertice, i, parametriccoord[0]));
       }
       // 0<s(1)<1 , intersezione di tipo inner
       else if((parametriccoord(1) > toll) && (parametriccoord(1) < 1.0 - toll))
       {
         newpoint.X = SegmentiPoligono[i].A.X + (SegmentiPoligono[i].B.X - SegmentiPoligono[i].A.X)*parametriccoord(1);
         newpoint.Y = SegmentiPoligono[i].A.Y + (SegmentiPoligono[i].B.Y - SegmentiPoligono[i].A.Y)*parametriccoord(1);
         // Come da requirements andiamo a modificare il vettore di punti del poligono
         PuntiPoligono.push_back(newpoint);
         // Impostiamo come numero del vertice il numero successivo all'ultimo vertice del poligono
         unsigned int numeronuovovertice = PuntiPoligono.size()-1;
         // Ci salviamo in un vettore tutti i punti del segmento con le informazioni di dove interseca e di dove si trova sulla retta
         PuntiSegmento.push_back(PuntiRettaIntersez(newpoint, numeronuovovertice, i, parametriccoord[0]));
       }
   }

   // Vedo se A e B sono esterni o interni al poligono, nel primo caso non li aggiungiamo al vettore di punti
   // nel secondo sì.
   // Algoritmo Ray-Casting

   // Andiamo a creare il sistema del tipo A1x = c per l'estremo A
   // e B1x=c per l'estremo B e contiamo le intersezioni

   Matrix2d inva1 , invb1;
   inva1(1,0) = -(Segment.A.Y - PuntoEst.Y);
   inva1(1,1) = (Segment.A.X - PuntoEst.X);
   invb1(1,0) = -(Segment.B.Y - PuntoEst.Y);
   invb1(1,1) = (Segment.B.X - PuntoEst.X);
   Vector2d parametriccoorda1 , parametriccoordb1;
   Vector2d c;
   int interseza = 0;
   int intersezb = 0;

   for (unsigned int i=0; i<Poligono.SegmentiPoligono.size(); i++){
       inva1(0,1)=-(SegmentiPoligono[i].A.X - SegmentiPoligono[i].B.X);
       inva1(0,0)= SegmentiPoligono[i].A.Y - SegmentiPoligono[i].B.Y;
       invb1(0,1)=-(SegmentiPoligono[i].A.X - SegmentiPoligono[i].B.X);
       invb1(0,0)= SegmentiPoligono[i].A.Y - SegmentiPoligono[i].B.Y;
       c(0) = (SegmentiPoligono[i].A.X - PuntoEst.X);
       c(1) = (SegmentiPoligono[i].A.Y - PuntoEst.Y);
       parametriccoorda1 = inva1 * c;
       parametriccoordb1 = invb1 * c;
       parametriccoorda1 /= inva1.determinant();
       parametriccoordb1 /= invb1.determinant();

       // Valutiamo se c'è e dove si trova l'intersezione. Se è di tipo Inner o End RISPETTO AD ENTRAMBI i segmenti
       // Allora aumentiamo di 1 il counter
       if ((parametriccoorda1(1) > 0.0 + toll) && (parametriccoorda1(0) > 0.0 + toll) && (parametriccoorda1(1) <= 1.0 + toll) && (parametriccoorda1(0) <= 1.0 + toll))
           interseza++;

       if ((parametriccoordb1(1) > 0.0 + toll) && (parametriccoordb1(0) > 0.0 + toll) && (parametriccoordb1(1) <= 1.0 + toll) && (parametriccoordb1(0) <= 1.0 + toll))
           intersezb++;
   }

   unsigned int flaga=0;
   unsigned int flagb=0;
   // Primo if: Valutiamo se il punto è interno (numero di intersezioni dispari)
   if(interseza%2 != 0){
       for(unsigned int i=0; i<PuntiSegmento.size(); i++){
           // Secondo if: Valutiamo se il punto coincide con un punto di intersezione già trovato
           if(Segment.A == PuntiSegmento[i].p){
               flaga = 1;
               break;
           }
       }
       // Se il punto è interno e non coincide con nessun punto di intersezione già trovato
       // allora lo aggiungiamo in entrambi i vettori
       if(flaga!=1){
       PuntiPoligono.push_back(Segment.A);
       // Per gli estremi A e B, poniamo lato intersezione uguale ad un numero che non è possibile rappresenti un'intersezione
       PuntiSegmento.push_back(PuntiRettaIntersez(Segment.A, PuntiPoligono.size()-1, SegmentiPoligono.size()+1, 0.00));
       }
   }

   // Ripetiamo lo stesso per l'estremo B
   if(intersezb%2 != 0){
       for(unsigned int i=0; i<PuntiSegmento.size(); i++){
           if(Segment.B == PuntiSegmento[i].p){
               flagb = 1;
               break;
           }
       }
       if(flagb!=1){
       PuntiPoligono.push_back(Segment.B);
       PuntiSegmento.push_back(PuntiRettaIntersez(Segment.B, PuntiPoligono.size()-1, SegmentiPoligono.size()+1, 1.00));
       }
   }


   //Ordinamento crescente vettore PuntiSegmento
   for(unsigned int i=0; i<PuntiSegmento.size(); i++){
       for(unsigned int j=i+1; j<PuntiSegmento.size(); j++){
           if(PuntiSegmento[i].CoordPar > PuntiSegmento[j].CoordPar){
               PuntiSegmento.push_back(PuntiSegmento[i]);
               PuntiSegmento[i] = PuntiSegmento[j];
               PuntiSegmento[j] = PuntiSegmento[PuntiSegmento.size()-1];
               PuntiSegmento.pop_back();
           }
       }
   }

   // Suddivisione dei punti originari del poligono in base alla loro posizione rispetto al tagliante
   vector<Vertex> verticidx;
   vector<Vertex> verticisx;

   // coordinate vettore V=AB
   double Vx = (Segment.B.X - Segment.A.X);
   double Vy = (Segment.B.Y - Segment.A.Y);

   unsigned int flag = 0; // 1 Se i-1 è a SX, 0 se i-1 è a DX

   // Ciclo sui punti originari del Poligono
   for(unsigned int i=0; i<VerticiPoligono.size(); i++){
      // z=VxWy-WxVy, ove W=Punto[i]-A
      double z = (Vx*(VerticiPoligono[i].P.Y - Segment.A.Y)) - ((VerticiPoligono[i].P.X - Segment.A.X)*Vy);

      // se z>0 il punto è a sx del tagliante
      if(z>toll){
          verticisx.push_back(VerticiPoligono[i]);
          flag = 1;
      }
      // se z<0 il punto è a dx del tagliante
      else if(z<-toll){
          verticidx.push_back(VerticiPoligono[i]);
          flag = 0;
      }
      // se z=0 guardiamo il flag e classifichiamo il punto come quello precedente
      else if(z>(-toll) && z<(toll)){
          if(flag == 1)
              verticisx.push_back(VerticiPoligono[i]);
          else if(flag == 0)
              verticidx.push_back(VerticiPoligono[i]);
      }
   }

   // INIZIO ALGORITMO DI TAGLIO

   vector<Polygon> nuovipoligoni;
   vector<Vertex> verticinuovopoligono; // Vettore d'appoggio per creazione dei sottopoligoni

   int partenzapoligono = -1; // Indica l'inizio del sottopoligono
   unsigned int dxsxflag = 0; // Si attiva se il punto d'intersezione è un vertice di sx
   unsigned int positionflag = 0; // Si attiva se il punto del segmento è un punto d'intersezione
   unsigned int endflag = 0; // Si attiva se vogliamo uscire dal ciclo di k e quindi di j

   for(unsigned int i=0; i<verticidx.size(); i++){
       //aggiungiamo vertici dx se NON coincidono con un punto in PuntiSegmento
       unsigned int IntersectionFlag = 0; // Si attiva se Vertice considerato coincide con un punto d'intersezione
       for(unsigned int l=0; l<PuntiSegmento.size(); l++){
           if(verticidx[i].numeroVertice == PuntiSegmento[l].numVertice){
               IntersectionFlag = 1;
               break;
           }
       }

       if(IntersectionFlag!=1){
           verticinuovopoligono.push_back(verticidx[i]);
           if(partenzapoligono == -1){
               if(verticidx[i].numeroVertice != 0)
                   partenzapoligono = verticidx[i].numeroVertice-1;
               else
                   partenzapoligono = SegmentiPoligono.size()-1;
           }

       for(unsigned int j=0; j<PuntiSegmento.size(); j++){
           // Verifichiamo se c'è intersezione sul segmento che parte dal vertice considerato
           if(verticidx[i].numeroVertice == PuntiSegmento[j].latoIntersez){
               //Se c'è intersezione aggiungiamo il punto d'intersezione al sottopoligono
               verticinuovopoligono.push_back(Vertex(PuntiSegmento[j].p , PuntiSegmento[j].numVertice));

               //Scorriamo all'indietro lungo la retta del segmento tagliante (POICHE' SIAMO SUI VERTICI A DX DEL SEGMENTO)
               for(unsigned int k=j-1; k>=0; k--){
                   // AGGIUNGO IL PUNTO
                   verticinuovopoligono.push_back(Vertex(PuntiSegmento[k].p , PuntiSegmento[k].numVertice));

                   //Verifico se il punto che stiamo aggiungendo è un vertice di sx
                   for(unsigned int u=0; u<verticisx.size(); u++){
                       if(PuntiSegmento[k].numVertice == verticisx[u].numeroVertice){
                           dxsxflag = 1;
                           break;
                           }
                       else
                           dxsxflag=0;
                    }

                   // Se sono su 'PartenzaPoligono' creo il nuovo poligono
                   // Controlliamo anche il numero del vertice in caso di intersezione di tipo End
                   if((PuntiSegmento[k].latoIntersez == partenzapoligono) || (PuntiSegmento[k].numVertice == partenzapoligono)){
                       nuovipoligoni.push_back(Polygon(verticinuovopoligono));
                       verticinuovopoligono.clear();
                       partenzapoligono = -1;
                       endflag = 1;
                       break; //USCIAMO DAL CICLO DI K
                   }

                   // Verifico se il punto che sto aggiungendo è d'intersezione e non è un vertice di sx
                   // in tal caso allora finisco di scorrere sulla retta del tagliante e torno a scorrere
                   // sul poligono
                   if((PuntiSegmento[k].latoIntersez != SegmentiPoligono.size()+1) && (dxsxflag==0)){
                       positionflag = 1;
                       endflag = 1;
                       break; // USCIAMO DAL CICLO DI K
                   }

                   // Se k=0 siamo sull'ultimo punto di PuntiSegmento, quindi torniamo a scorrere sul poligono
                   if(k==0){
                       positionflag = 1;
                       endflag = 1;
                   }

               } //CHIUSURA CICLO K

               if(endflag == 1){
                   endflag = 0;
                   break; //USCIAMO DAL CICLO DI J
                   }
           }

           else{
               //Creiamo comunque il poligono quando siamo sull'ultimo vertice di dx e non ci sono più intersezioni
               if((positionflag == 1) && (i==verticidx.size()-1) && (j==verticidx.size()-1)){
                   nuovipoligoni.push_back(Polygon(verticinuovopoligono));
                   verticinuovopoligono.clear();
                   partenzapoligono = -1;
                   break;
               }
           }

           }
       }

   }

   partenzapoligono = -1;
   dxsxflag = 0;
   positionflag = 0;
   endflag = 0;

   for(unsigned int i=0; i<verticisx.size(); i++){
       //AGGIUNGO VERTICI SX SE NON E' COINCIDENTE CON UNO DI INTERSEZIONE
       unsigned int intersectionflag = 0;
       for(unsigned int l=0; l<PuntiSegmento.size(); l++){
           if(verticisx[i].numeroVertice == PuntiSegmento[l].numVertice){
               intersectionflag = 1;
               break;
           }
       }

       if(intersectionflag!=1){
       verticinuovopoligono.push_back(verticisx[i]);

       if(partenzapoligono == -1){
           if(verticisx[i].numeroVertice != 0)
               partenzapoligono = verticisx[i].numeroVertice-1;
           else
               partenzapoligono = SegmentiPoligono.size()-1;
       }


       for(unsigned int j=0; j<PuntiSegmento.size(); j++){
           if(verticisx[i].numeroVertice == PuntiSegmento[j].latoIntersez){
               verticinuovopoligono.push_back(Vertex(PuntiSegmento[j].p , PuntiSegmento[j].numVertice));

               //Scorro in avanti sulla retta del tagliante (POICHE' SIAMO SUI VERTICI A SX DEL SEGMENTO TAGLIANTE)
               for(unsigned int k=j+1; k<PuntiSegmento.size(); k++){
                   verticinuovopoligono.push_back(Vertex(PuntiSegmento[k].p , PuntiSegmento[k].numVertice));

                   //Verifico se il punto che sto aggiungendo è un vertice di dx
                   for(unsigned int u=0; u<verticidx.size(); u++){
                       if(PuntiSegmento[k].numVertice == verticidx[u].numeroVertice){
                           dxsxflag = 1;
                       break;
                       }
                       else
                           dxsxflag=0;
                }

                   if((PuntiSegmento[k].latoIntersez == partenzapoligono) || (PuntiSegmento[k].numVertice == partenzapoligono)){
                       nuovipoligoni.push_back(Polygon(verticinuovopoligono));
                       verticinuovopoligono.clear();
                       partenzapoligono = -1;
                       endflag = 1;
                       break; //USCIAMO DAL CICLO DI K
                   }

                   // VERIFICO SE PUNTO CHE STO AGGIUNGENDO E' D'INTERSEZIONE E NON E' UN VERTICE DI DX
                   // IN TAL CASO ALLORA FINISCO DI SCORRERE SULLA RETTA DEL TAGLIANTE E TORNO A SCORRERE
                   // SUL POLIGONO
                   if((PuntiSegmento[k].latoIntersez != SegmentiPoligono.size()+1) && (dxsxflag==0)){
                       positionflag = 1;
                       endflag = 1;
                       break; // USCIAMO DAL CICLO DI K
                   }

               }

               if(endflag == 1){
                   endflag = 0;
                   break; //USCIAMO DAL CICLO DI J
                   }
           }

           else{
               //CREIAMO COMUNQUE IL POLIGONO QUANDO SIAMO SULL'ULTIMO VERTICE DI SX E NON CI SONO PIU' INTERSEZIONI
               if((positionflag == 1) && (i==verticisx.size()-1) && (j==verticisx.size()-1)){
                   nuovipoligoni.push_back(Polygon(verticinuovopoligono));
                   verticinuovopoligono.clear();
                   break;
               }
           }

           }
       }

   }

   return nuovipoligoni;
}

// FINE PROGETTO 1 - - - - - - - - - - - - - - -

Polygon Polygon::CreateBoundingBox()
{
    //algoritmo per creare il bounding box del poligono
    //si individuano le coordinate minime e massime dei punti del poligono
    double Xmin = PuntiPoligono[0].X;
    double Ymin = PuntiPoligono[0].Y;
    for(unsigned int i=1; i<PuntiPoligono.size(); i++){
        if(PuntiPoligono[i].X < Xmin)
            Xmin = PuntiPoligono[i].X;
        if(PuntiPoligono[i].Y < Ymin)
            Ymin = PuntiPoligono[i].Y;
    }

    double Xmax = PuntiPoligono[0].X;
    double Ymax = PuntiPoligono[0].Y;
    for(unsigned int i=1; i<PuntiPoligono.size(); i++){
        if(PuntiPoligono[i].X > Xmax)
            Xmax = PuntiPoligono[i].X;
        if(PuntiPoligono[i].Y > Ymax)
            Ymax = PuntiPoligono[i].Y;
    }

    //numerazione dei vertici proseguendo quella del poligono
    vector<Vertex> VerticiBounding;
    VerticiBounding.push_back(Vertex(Point(Xmin,Ymin), PuntiPoligono.size()));
    VerticiBounding.push_back(Vertex(Point(Xmax,Ymin), PuntiPoligono.size()+1));
    VerticiBounding.push_back(Vertex(Point(Xmax,Ymax), PuntiPoligono.size()+2));
    VerticiBounding.push_back(Vertex(Point(Xmin,Ymax), PuntiPoligono.size()+3));

    Polygon BoundingBox = Polygon(VerticiBounding);
    return BoundingBox;
}

vector<Polygon> Polygon::PoligoniInBounding()
{
    //serve per torvare le varie mattonelle che andranno a comporre il ReferenceElement
    vector<Vertex> VerticiNuovoPoligono;
    vector<Polygon> SottoPoligoni;
    vector<PuntiRettaIntersez> PuntiIntersezione; //conterrà le intersezioni tra il poligono e il suo bounding box

    Polygon BoundingBox = CreateBoundingBox();
    SottoPoligoni.push_back(Polygon(VerticiPoligono)); //aggiungiamo come prima mattonella il poligono di partenza

    //ricerca delle interseioni tra poligono e il bounding box
    for(unsigned int i=0; i<VerticiPoligono.size(); i++){
        for(unsigned int j=0; j<BoundingBox.SegmentiPoligono.size(); j++){

            //se il lato del bounding box ha numerazione pari, allora è orizzontale:
            //in tal caso per le intersezioni basta confrontare le ordinate
            if(j%2==0){
               if(VerticiPoligono[i].P.Y == BoundingBox.SegmentiPoligono[j].A.Y){
                   PuntiIntersezione.push_back(PuntiRettaIntersez(VerticiPoligono[i].P, VerticiPoligono[i].numeroVertice, j, 0.00));
                   break;
               }
           }

           //se il lato del bounding box ha numerazione dispari, allora è verticale:
           //in tal caso per le intersezioni basta confrontare le ascisse
           else {
               if(VerticiPoligono[i].P.X == BoundingBox.SegmentiPoligono[j].A.X){
                   PuntiIntersezione.push_back(PuntiRettaIntersez(VerticiPoligono[i].P, VerticiPoligono[i].numeroVertice, j, 0.00));
                   break;
               }
           }
        }
    }

    //ordinamento crescente dei punti appena trovati in base al lato di intersezione
    for(unsigned int i=0; i<PuntiIntersezione.size(); i++){
        for(unsigned int j=i+1; j<PuntiIntersezione.size(); j++){
            if(PuntiIntersezione[i].latoIntersez >PuntiIntersezione[j].latoIntersez){
                PuntiIntersezione.push_back(PuntiIntersezione[i]);
                PuntiIntersezione[i] = PuntiIntersezione[j];
                PuntiIntersezione[j] = PuntiIntersezione[PuntiIntersezione.size()-1];
                PuntiIntersezione.pop_back();
            }
        }
    }

    //CREAZIONE DEI SOTTOPOLIGONI
    for(unsigned int i=0; i<PuntiIntersezione.size(); i++){

        //primo if: controlliamo che il punto di intersezione non sia l'ultimo, questo caso verrà trattato in seguito
        if(i!=PuntiIntersezione.size()-1){
            //secondo if: verifichiamo se due punti di intersezione consecutivi sono situati sullo stesso lato del BB
            if(PuntiIntersezione[i+1].latoIntersez == PuntiIntersezione[i].latoIntersez){
                //terzo if: un lato del poligono coincide con uno del BB? Nel caso, non creiamo il poligono
                if(PuntiIntersezione[i+1].numVertice == PuntiIntersezione[i].numVertice+1)
                    break;
                //in caso contrario, creiamo il poligono
                else{
                    //aggiungiamo i due punti di intersezione e, con il for, tutti i punti compresi tra di essi in senso antiorario
                    VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[i].p,PuntiIntersezione[i].numVertice));
                    VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[i+1].p,PuntiIntersezione[i+1].numVertice));
                    for(unsigned int k=PuntiIntersezione[i+1].numVertice-1; k>PuntiIntersezione[i].numVertice; k--)
                        VerticiNuovoPoligono.push_back(VerticiPoligono[k]);

                    SottoPoligoni.push_back(Polygon(VerticiNuovoPoligono));
                    VerticiNuovoPoligono.clear();
                }
            }

            //else riferito al secondo if, ovvero se due punti di intersezione consecutivi stanno su lati diversi del BB
            else{
                //aggiungiamo il "primo" punto di intersezione
                VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[i].p,PuntiIntersezione[i].numVertice));

                //aggiungiamo tutti i vertici del BOUNDING BOX compresi tra i due punti di intersezione
                for(unsigned int u=PuntiIntersezione[i].latoIntersez; u<PuntiIntersezione[i+1].latoIntersez; u++)
                     VerticiNuovoPoligono.push_back(BoundingBox.VerticiPoligono[u+1]);

                //aggiungiamo il "secondo" punto di intersezione
                VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[i+1].p,PuntiIntersezione[i+1].numVertice));

                //aggiungiamo tutti i punti compresi tra di essi in senso antiorario
                for(unsigned int k=PuntiIntersezione[i+1].numVertice-1; k>PuntiIntersezione[i].numVertice; k--)
                    VerticiNuovoPoligono.push_back(VerticiPoligono[k]);

                SottoPoligoni.push_back(Polygon(VerticiNuovoPoligono));
                VerticiNuovoPoligono.clear();
            }
        }

        //se siamo sull'ultimo punto di intersezione, eseguiamo le stesse operazioni ma sostituendo 0 a i+1
        else{
            if(PuntiIntersezione[0].latoIntersez == PuntiIntersezione[i].latoIntersez){
                    if(PuntiIntersezione[0].numVertice == PuntiIntersezione[i].numVertice+1)
                        break;
                    else{
                        VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[i].p,PuntiIntersezione[i].numVertice));
                        VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[0].p,PuntiIntersezione[0].numVertice));
                        if(PuntiIntersezione[0].numVertice != 0){
                            for(unsigned int k=PuntiIntersezione[0].numVertice-1; k>=0; k--)
                                VerticiNuovoPoligono.push_back(VerticiPoligono[k]);
                        }

                        for(unsigned int k=VerticiPoligono.size()-1; k>PuntiIntersezione[i].numVertice; k--)
                            VerticiNuovoPoligono.push_back(VerticiPoligono[k]);

                        SottoPoligoni.push_back(Polygon(VerticiNuovoPoligono));
                        VerticiNuovoPoligono.clear();
                    }
            }
            else{
                VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[i].p,PuntiIntersezione[i].numVertice));
                VerticiNuovoPoligono.push_back(BoundingBox.VerticiPoligono[0]);
                VerticiNuovoPoligono.push_back(Vertex(PuntiIntersezione[0].p,PuntiIntersezione[0].numVertice));
                if(PuntiIntersezione[0].numVertice != 0){
                    //se il primo punto di intersezione nel vettore non è il vertice zero del poligono: allora scorriamo all'indietro aggiungendo
                    //tutti i punti compresi tra i punti di intersezione considerati, fermandoci al vertice zero
                    for(unsigned int k=PuntiIntersezione[0].numVertice-1; k>=0; k--)
                        VerticiNuovoPoligono.push_back(VerticiPoligono[k]);
                }

                //se l'ultimo punto di intersezione del vettore non coincide con l'ultimo vertice del poligono, partiamo dall'ultimo vertice del poligono
                //e aggiungiamo tutti i punti fino ad arrivare a quello di intersezione
                for(unsigned int k=VerticiPoligono.size()-1; k>PuntiIntersezione[i].numVertice; k--)
                    VerticiNuovoPoligono.push_back(VerticiPoligono[k]);

                SottoPoligoni.push_back(Polygon(VerticiNuovoPoligono));
                VerticiNuovoPoligono.clear();
            }
        }
    }

    return SottoPoligoni;

}

//costruttore del reference element
Polygon::ReferenceElement::ReferenceElement(vector<Polygon> Poligoni)
{
    PoligoniRE = Poligoni;
}

//costruttore copia del reference element
Polygon::ReferenceElement::ReferenceElement(const Polygon::ReferenceElement &RE)
{
    PoligoniRE = RE.PoligoniRE;
}

//funzione per creare un reference element a partire da un poligono
Polygon::ReferenceElement Polygon::CreateReferenceElement()
{
    //attraverso le funzioni definite in precedenza, creiamo il bounding box e i sottopoligoni
    Polygon BoundingBox = CreateBoundingBox();
    vector<Polygon> PoligoniRE = PoligoniInBounding();
    PoligoniRE.push_back(BoundingBox);
    //il reference element sarà quindi un vettore di poligoni che avrà come primo poligono il poligono principale e come ultimo il bounding box
    return ReferenceElement(PoligoniRE);
}


double Polygon::ComputeArea()
{
    //sfruttiamo la formula di gauss per calcolare l'area di un singolo poligono
    double AreaPoligono = 0;
    for(unsigned int j=0; j<PuntiPoligono.size(); j++){
        if(j!=PuntiPoligono.size()-1){
            AreaPoligono+=abs(PuntiPoligono[j].X*PuntiPoligono[j+1].Y);
            AreaPoligono-= abs(PuntiPoligono[j].Y*PuntiPoligono[j+1].X);
        }
        else {
            AreaPoligono+= abs(PuntiPoligono[j].X*PuntiPoligono[0].Y);
            AreaPoligono-= abs(PuntiPoligono[j].Y*PuntiPoligono[0].X);
                }
    }
    return AreaPoligono/2;
}


double Polygon::ReferenceElement::ComputeAreaRE()
{
    //calcoliamo l'area del reference element sommando le aree di tutti i poligoni che lo compongono, eccezion fatta per il bounding box
    double AreaTOT=0;
    for(unsigned int i=0; i<PoligoniRE.size()-1; i++)
        AreaTOT = AreaTOT + PoligoniRE[i].ComputeArea();
    return AreaTOT;
}

Polygon::ReferenceElement Polygon::TranslateDx(const Polygon::ReferenceElement &RE, const double deltaX)
{
    //trasliamo a destra tutte le componenti di ciascun poligono del reference element
    ReferenceElement REdx = ReferenceElement(RE);

    for(unsigned int i=0; i<REdx.PoligoniRE.size(); i++){
        for(unsigned int j=0; j<REdx.PoligoniRE[i].VerticiPoligono.size(); j++){
            REdx.PoligoniRE[i].VerticiPoligono[j].P.X += deltaX;
            REdx.PoligoniRE[i].PuntiPoligono[j].X += deltaX;
            REdx.PoligoniRE[i].SegmentiPoligono[j].A.X += deltaX;
            REdx.PoligoniRE[i].SegmentiPoligono[j].B.X += deltaX;
        }
    }

    return REdx;
}

Polygon::ReferenceElement Polygon::TranslateUp(const Polygon::ReferenceElement &RE, const double deltaY)
{
    //trasliamo verso l'alto tutte le componenti di ciascun poligono del reference element
    ReferenceElement REup = ReferenceElement(RE);

    for(unsigned int i=0; i<REup.PoligoniRE.size(); i++){
        for(unsigned int j=0; j<REup.PoligoniRE[i].VerticiPoligono.size(); j++){
            REup.PoligoniRE[i].VerticiPoligono[j].P.Y += deltaY;
            REup.PoligoniRE[i].PuntiPoligono[j].Y += deltaY;
            REup.PoligoniRE[i].SegmentiPoligono[j].A.Y += deltaY;
            REup.PoligoniRE[i].SegmentiPoligono[j].B.Y += deltaY;
        }
    }

    return REup;

}

//CREIAMO LA MESH DEL DOMINIO
vector<Polygon::ReferenceElement> Polygon::CreateMesh(Polygon::ReferenceElement RE, Polygon dominio)
{
    vector<ReferenceElement> vectorRef; //vettore dei reference element che compongono la mesh

    //i seguenti vettori saranno di supporto alla funzione
    vector<unsigned int> numvertici;
    vector<Polygon> poligonitagliati;
    vector<Polygon> sottopoligoni;
    vector<ReferenceElement> ultimariga;
    int n = RE.PoligoniRE.size();
    double deltax, deltay;

    RE = CreateReferenceElement(); //ci creiamo il reference element del poligono che chiama la funzione

    //trasliamo il reference element nell'origine
    deltax = RE.PoligoniRE[n-1].PuntiPoligono[0].X;
    deltay = RE.PoligoniRE[n-1].PuntiPoligono[0].Y;
    vectorRef.push_back(TranslateUp(TranslateDx(RE, -deltax), -deltay));

    //ridefiniamo deltax e deltay come lunghezza e altezza del BB
    deltax = RE.PoligoniRE[n-1].PuntiPoligono[1].X - RE.PoligoniRE[n-1].PuntiPoligono[0].X;
    deltay = RE.PoligoniRE[n-1].PuntiPoligono[2].Y - RE.PoligoniRE[n-1].PuntiPoligono[1].Y;

    //trasliamo il reference element verso destra finchè il vertice a sinistra del BB risulta dentro il dominio
    for(unsigned int i=1; (vectorRef[i-1].PoligoniRE[vectorRef[i-1].PoligoniRE.size()-1].PuntiPoligono[0].X) + deltax < dominio.PuntiPoligono[1].X; i++){
        vectorRef.push_back(TranslateDx(vectorRef[i-1], deltax));

        //verifichiamo se il vertice di destra del BB del reference element traslato va oltre il dominio: in tal caso effettuiamo il taglio
        if(vectorRef[i].PoligoniRE[vectorRef[i-1].PoligoniRE.size()-1].PuntiPoligono[1].X > dominio.PuntiPoligono[1].X){

            //applichiamo la funzione CutPolygon ad ogni poligono del reference element
            for(unsigned int j=0; j<vectorRef[i].PoligoniRE.size(); j++){
               for(unsigned int k=0; k<vectorRef[i].PoligoniRE[j].VerticiPoligono.size(); k++)
                   numvertici.push_back(k);
              poligonitagliati = vectorRef[i].PoligoniRE[j].CutPolygon(vectorRef[i].PoligoniRE[j].PuntiPoligono, numvertici, dominio.SegmentiPoligono[1]);

              //se il vettore PoligoniTagliati risulta vuoto, verifichiamo se il poligono considerato è interamente a sinistra o destra della retta tagliante
              if(poligonitagliati.size()==0){
                  for(unsigned int v=0; v<vectorRef[i].PoligoniRE[j].PuntiPoligono.size(); v++){

                      //se è situato a sinistra: lo aggiungiamo al vettore sottopoligoni
                      if(vectorRef[i].PoligoniRE[j].PuntiPoligono[v].X < dominio.SegmentiPoligono[1].A.X){
                          sottopoligoni.push_back(vectorRef[i].PoligoniRE[j]);
                          break;
                      }

                      //se è a destra: non lo consideriamo
                      else if(vectorRef[i].PoligoniRE[j].PuntiPoligono[v].X  > dominio.SegmentiPoligono[1].A.X)
                          break;
                  }
              }
              numvertici.clear();
              //se poligoni tagliati non è vuoto: allora aggiungiamo i poligoni di sinistra e non teniamo conto di quelli a destra
              for(unsigned int u=0; u<poligonitagliati.size(); u++){
                  for(unsigned int v=0; v<poligonitagliati[u].PuntiPoligono.size(); v++){
                      if(poligonitagliati[u].PuntiPoligono[v].X < dominio.SegmentiPoligono[1].A.X){
                          sottopoligoni.push_back(poligonitagliati[u]);
                          break;
                      }
                      else if(poligonitagliati[u].PuntiPoligono[v].X > dominio.SegmentiPoligono[1].A.X)
                          break;
              }

           }
           poligonitagliati.clear();
        }

         //sostituiamo l'ultimo elemento della mesh con il reference element appena tagliato
         vectorRef.pop_back();
         vectorRef.push_back(ReferenceElement(sottopoligoni));
         sottopoligoni.clear();
        }
        }

    unsigned int numeroREsuriga = vectorRef.size();

    //trasliamo tutta la riga dei reference element verso l'alto fino a che il vertice in basso del BB non supera l'altezza del dominio
    //ad ogni iterazione aumentiamo la i ddel numero di reference element su riga
    for(unsigned int i=1; (vectorRef[i-1].PoligoniRE[vectorRef[i-1].PoligoniRE.size()-1].PuntiPoligono[0].Y + deltay) < dominio.PuntiPoligono[2].Y; i+=numeroREsuriga){

        //questo for ci serve per traslare verso l'alto tutta la riga di reference element
        for(unsigned int j=0; j<numeroREsuriga; j++)
            vectorRef.push_back(TranslateUp(vectorRef[j+i-1], deltay));

        //se il vertice in alto del BB supera l'altezza del dominio, allora effettuiamo il taglio
        if((vectorRef[i-1].PoligoniRE[vectorRef[i-1].PoligoniRE.size()-1].PuntiPoligono[3].Y + deltay) > dominio.PuntiPoligono[2].Y){

            for(unsigned int e=0; e<numeroREsuriga; e++){  //ci indica quale reference element della riga stiamo considerando
                for(unsigned int t=0; t<vectorRef[i+numeroREsuriga+e-1].PoligoniRE.size(); t++){  //ci indica il poligono del reference element considerto
                    for(unsigned int k=0; k<vectorRef[t].PoligoniRE[t].VerticiPoligono.size(); k++)
                        numvertici.push_back(k);
               poligonitagliati = vectorRef[i+numeroREsuriga+e-1].PoligoniRE[t].CutPolygon(vectorRef[i+numeroREsuriga+e-1].PoligoniRE[t].PuntiPoligono, numvertici, dominio.SegmentiPoligono[2]); //ho messo il segmento tagliante giusto

               //divisione in destra e sinistra dei poligoni appena taglliati
               if(poligonitagliati.size()==0){
                   for(unsigned int v=0; v<vectorRef[i+numeroREsuriga+e-1].PoligoniRE[t].PuntiPoligono.size(); v++){
                   if(vectorRef[i+numeroREsuriga+e-1].PoligoniRE[t].PuntiPoligono[v].Y < dominio.SegmentiPoligono[2].A.Y){
                       sottopoligoni.push_back(vectorRef[i+numeroREsuriga+e-1].PoligoniRE[t]);
                       break;
                   }
                   else if(vectorRef[i+numeroREsuriga+e-1].PoligoniRE[t].PuntiPoligono[v].Y  > dominio.SegmentiPoligono[2].A.Y){
                       break;
                      }
                   }
               }
               numvertici.clear();
               for(unsigned int u=0; u<poligonitagliati.size(); u++){
                   for(unsigned int v=0; v<poligonitagliati[u].PuntiPoligono.size(); v++){
                       if(poligonitagliati[u].PuntiPoligono[v].Y < dominio.SegmentiPoligono[2].A.Y){
                           sottopoligoni.push_back(poligonitagliati[u]);
                           break;
                       }
                       else if(poligonitagliati[u].PuntiPoligono[v].Y > dominio.SegmentiPoligono[2].A.Y)
                           break;
               }
            }

            poligonitagliati.clear();
                }

                //aggiungiamo il reference element tagliato al vettore di supporto ultimariga
                ultimariga.push_back(ReferenceElement(sottopoligoni));
                sottopoligoni.clear();
         }
    }

    }

    //sostituiamo l'ultima riga di reference element con il vettore ultimariga, contenente i reference element tagliati
    for(unsigned int i=0; i<ultimariga.size(); i++){
        vectorRef.pop_back();
    }
    for(unsigned int j=0; j<ultimariga.size(); j++){
        vectorRef.push_back(ultimariga[j]);
    }

    return vectorRef;
}

}
