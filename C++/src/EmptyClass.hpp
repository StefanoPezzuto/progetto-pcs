#ifndef EMPTYCLASS_H
#define EMPTYCLASS_H
#include <iostream>
#include <vector>
#include "Eigen"
using namespace Eigen;

using namespace std;

namespace ProjectNamespace {

class Point {
  public:
    double X;
    double Y;

    Point(const double& x,
          const double& y);
    Point(const Point& point);

    Point operator =(const Point& point);
    bool operator ==(const Point& point) const;

      };

class Vertex {
  public:
    Point P;
    unsigned int numeroVertice;

    Vertex (const Point& p, const unsigned int& n);
    Vertex (const Vertex& vertice);
    bool operator ==(const Vertex& vertex) const;

};

class Segment {
  public:
    Point A;
    Point B;

    Segment (const Point &p1,
             const Point &p2);
    Segment(const Segment& segment);
};


class PuntiRettaIntersez{
  public:
    Point p;
    unsigned int numVertice;
    unsigned int latoIntersez;
    float CoordPar;

    PuntiRettaIntersez(const Point Punto, const unsigned int n, const unsigned int l, const float c);
    PuntiRettaIntersez(const PuntiRettaIntersez& puntorettaintersez);
    PuntiRettaIntersez operator =(PuntiRettaIntersez& puntorettaintersez);

};

class Polygon {
  public:
    vector<Point> PuntiPoligono;
    vector<Segment> SegmentiPoligono;
    vector<Vertex> VerticiPoligono;

    Polygon(vector<Vertex> Vertici);
    Polygon(const Polygon& polygon);
    bool operator ==(const Polygon& polygon) const;

  public:
    vector<Polygon> CutPolygon(vector<Point> &Points, const vector<unsigned int> Vertices, const Segment Segmento);

    // FINE PROGETTO 1

    class ReferenceElement{
    public:
        vector<Polygon> PoligoniRE;
  public:
        ReferenceElement(vector<Polygon> Poligoni);
        ReferenceElement(const ReferenceElement& RE);
        double ComputeAreaRE();
    };

  public:
    Polygon CreateBoundingBox();
    vector<Polygon> PoligoniInBounding ();
    ReferenceElement CreateReferenceElement();
    double ComputeArea();
    ReferenceElement TranslateDx(const ReferenceElement& RE, const double deltaX);
    ReferenceElement TranslateUp(const ReferenceElement& RE, const double deltaY);
    vector<ReferenceElement> CreateMesh (ReferenceElement RE, Polygon dominio);
};

}

#endif // EMPTYCLASS_H

