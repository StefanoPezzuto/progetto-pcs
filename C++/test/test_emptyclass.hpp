#ifndef __TEST_EMPTYCLASS_H
#define __TEST_EMPTYCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "EmptyClass.hpp"

using namespace ProjectNamespace;
using namespace testing;
using namespace std;

namespace ProjectTesting {


  TEST(TestRettangolo, TestCutPolygon)
  {
    //test rettangolo
    vector<Point> PuntiRettangolo;
    vector<unsigned int> Indici = {0, 1, 2, 3};
    vector<double> coord = {1.0, 1.0, 5.0, 1.0, 5.0, 3.1, 1.0, 3.1};
    Segment segmento = Segment(Point(2.0,1.2),Point(4.0,3.0));
    for(unsigned int i=0; i<coord.size(); i+=2){
        PuntiRettangolo.push_back(Point(coord[i], coord[i+1]));
    }

    vector<Vertex> VerticiRettangolo;

    for(unsigned int i=0; i<Indici.size(); i++){
        VerticiRettangolo.push_back(Vertex(PuntiRettangolo[i], Indici[i]));
    }

    Polygon Rettangolo(VerticiRettangolo);
    vector<Polygon> PoligoniTagliati;

    try
    {
     PoligoniTagliati= Rettangolo.CutPolygon(Rettangolo.PuntiPoligono, Indici, segmento);
     vector<Point> NuoviPunti = PuntiRettangolo;
     vector<Vertex> NuoviVertici = VerticiRettangolo;
     NuoviPunti.push_back(Point(1.7777,1.0));
     NuoviVertici.push_back(Vertex(Point(1.7777,1.0), 4));
     NuoviPunti.push_back(Point(4.1111,3.1));
     NuoviVertici.push_back(Vertex(Point(4.1111,3.1), 5));
     NuoviPunti.push_back(Point(2.0,1.2));
     NuoviVertici.push_back(Vertex(Point(2.0,1.2), 6));
     NuoviPunti.push_back(Point(4.0,3.0));
     NuoviVertici.push_back(Vertex(Point(4.0,3.0), 7));
     for(unsigned int i=0; i<Rettangolo.PuntiPoligono.size(); i++)
         EXPECT_EQ(Rettangolo.PuntiPoligono[i] , NuoviPunti[i]);

     vector<Polygon> NuoviPoligoni;
     vector<Vertex> VerticiCostr = {NuoviVertici[1], NuoviVertici[2], NuoviVertici[5], NuoviVertici[7], NuoviVertici[6], NuoviVertici[4]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();
     VerticiCostr = {NuoviVertici[0], NuoviVertici[4], NuoviVertici[6], NuoviVertici[7], NuoviVertici[5], NuoviVertici[3]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     for(unsigned int i=0; i<PoligoniTagliati.size(); i++)
         EXPECT_EQ(PoligoniTagliati[i], NuoviPoligoni[i]);

    }

    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestPentagono, TestCutPolygon){

      vector<Point> PuntiPentagono;
      vector<unsigned int> Indici = {0, 1, 2, 3, 4};
      vector<double> coord = {2.5, 1.0, 4.0, 2.1, 3.4, 4.2, 1.6, 4.2, 1.0, 2.1};
      Segment segmento = Segment(Point(1.4,2.75),Point(3.6,2.2));
      for(unsigned int i=0; i<coord.size(); i+=2){
          PuntiPentagono.push_back(Point(coord[i], coord[i+1]));
      }

      vector<Vertex> VerticiPentagono;

      for(unsigned int i=0; i<Indici.size(); i++){
          VerticiPentagono.push_back(Vertex(PuntiPentagono[i], Indici[i]));
      }

      Polygon Pentagono(VerticiPentagono);
      vector<Polygon> PoligoniTagliati;

      try
      {
       PoligoniTagliati=Pentagono.CutPolygon(Pentagono.PuntiPoligono, Indici, segmento);
       vector<Point> NuoviPunti = PuntiPentagono;
       vector<Vertex> NuoviVertici = VerticiPentagono;

       NuoviPunti.push_back(Point(1.2, 2.8));
       NuoviVertici.push_back(Vertex(Point(1.2, 2.8), 5));
       NuoviPunti.push_back(segmento.A);
       NuoviVertici.push_back(Vertex(segmento.A , 6));
       NuoviPunti.push_back(segmento.B);
       NuoviVertici.push_back(Vertex(segmento.B , 7));

       for(unsigned int i=0; i<Pentagono.PuntiPoligono.size(); i++)
           EXPECT_EQ(Pentagono.PuntiPoligono[i] , NuoviPunti[i]);

       vector<Polygon> NuoviPoligoni;
       vector<Vertex> VerticiCostr = {NuoviVertici[0], NuoviVertici[1], NuoviVertici[7], NuoviVertici[6], NuoviVertici[5], NuoviVertici[4]};
       NuoviPoligoni.push_back(Polygon(VerticiCostr));
       VerticiCostr.clear();
       VerticiCostr = {NuoviVertici[2], NuoviVertici[3], NuoviVertici[5], NuoviVertici[6], NuoviVertici[7], NuoviVertici[1]};
       NuoviPoligoni.push_back(Polygon(VerticiCostr));
       VerticiCostr.clear();

       for(unsigned int i=0; i<PoligoniTagliati.size(); i++)
           EXPECT_EQ(PoligoniTagliati[i], NuoviPoligoni[i]);

      }

       catch (const exception& exception)
       {
         FAIL();
       }

  }


  TEST(TestConcEsagono, TestCutPolygon){

      vector<Point> PuntiConcavo;
      vector<unsigned int> Indici = {0, 1, 2, 3, 4, 5};
      vector<double> coord = {1.5, 1.0, 5.6, 1.5, 5.5, 4.8, 4.0, 6.2, 3.2, 4.2, 1.0, 4.0};
      Segment segmento = Segment(Point(2.0,3.7),Point(4.1,5.9));
      for(unsigned int i=0; i<coord.size(); i+=2){
          PuntiConcavo.push_back(Point(coord[i], coord[i+1]));
      }

      vector<Vertex> VerticiConcavo;

      for(unsigned int i=0; i<Indici.size(); i++){
          VerticiConcavo.push_back(Vertex(PuntiConcavo[i], Indici[i]));
      }

      Polygon Concavo(VerticiConcavo);
      vector<Polygon> PoligoniTagliati;

      try
      {
       PoligoniTagliati=Concavo.CutPolygon(Concavo.PuntiPoligono, Indici, segmento);
       vector<Point> NuoviPunti = PuntiConcavo;
       vector<Vertex> NuoviVertici = VerticiConcavo;
       NuoviPunti.push_back(Point(4.2043, 6.0092));
       NuoviVertici.push_back(Vertex(Point(4.2043, 6.0092) , 6));
       NuoviPunti.push_back(Point(3.7213, 5.5032));
       NuoviVertici.push_back(Vertex(Point(3.7213, 5.5032), 7));
       NuoviPunti.push_back(Point(2.4085, 4.1280));
       NuoviVertici.push_back(Vertex(Point(2.4085, 4.1280), 8));
       NuoviPunti.push_back(Point(1.1912, 2.8527));
       NuoviVertici.push_back(Vertex(Point(1.1912, 2.8527), 9));
       NuoviPunti.push_back(segmento.A);
       NuoviVertici.push_back(Vertex(segmento.A , 10));
       NuoviPunti.push_back(segmento.B);
       NuoviVertici.push_back(Vertex(segmento.B , 11));

       for(unsigned int i=0; i<Concavo.PuntiPoligono.size(); i++)
           EXPECT_EQ(Concavo.PuntiPoligono[i] , NuoviPunti[i]);


       vector<Polygon> NuoviPoligoni;
       vector<Vertex> VerticiCostr = {NuoviVertici[0], NuoviVertici[1], NuoviVertici[2], NuoviVertici[6], NuoviVertici[11], NuoviVertici[7], NuoviVertici[4], NuoviVertici[8], NuoviVertici[10], NuoviVertici[9]};
       NuoviPoligoni.push_back(Polygon(VerticiCostr));
       VerticiCostr.clear();
       VerticiCostr = {NuoviVertici[3], NuoviVertici[7], NuoviVertici[11], NuoviVertici[6]};
       NuoviPoligoni.push_back(Polygon(VerticiCostr));
       VerticiCostr.clear();
       VerticiCostr = {NuoviVertici[5], NuoviVertici[9], NuoviVertici[10], NuoviVertici[8]};
       NuoviPoligoni.push_back(Polygon(VerticiCostr));
       VerticiCostr.clear();

       for(unsigned int i=0; i<PoligoniTagliati.size(); i++)
           EXPECT_EQ(PoligoniTagliati[i], NuoviPoligoni[i]);

      }

      catch (const exception& exception)
      {
        FAIL();
      }
  }

  TEST(TestDecagono, TestCutPolygon)
  {
    vector<Point> PuntiDecagono;
    vector<unsigned int> Indici = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    vector<double> coord = {2.0, -2.0, 0.0, -1.0, 3.0, 1.0, 0.0, 2.0, 3.0, 2.0, 3.0, 3.0, -1.0, 3.0, -3.0, 1.0, 0.0, 0.0, -3.0, -2.0};
    Segment segmento = Segment(Point(0.0, -3.0),Point(0.0, 4.0));
    for(unsigned int i=0; i<coord.size(); i+=2){
        PuntiDecagono.push_back(Point(coord[i], coord[i+1]));
    }

    vector<Vertex> VerticiDecagono;

    for(unsigned int i=0; i<Indici.size(); i++){
        VerticiDecagono.push_back(Vertex(PuntiDecagono[i], Indici[i]));
    }

    Polygon Decagono(VerticiDecagono);
    vector<Polygon> PoligoniTagliati;

    try
    {
     PoligoniTagliati=Decagono.CutPolygon(Decagono.PuntiPoligono, Indici, segmento);
     vector<Point> NuoviPunti = PuntiDecagono;
     vector<Vertex> NuoviVertici = VerticiDecagono;
     NuoviPunti.push_back(Point(0.0,3.0));
     NuoviVertici.push_back(Vertex(Point(0.0,3.0), 10));
     NuoviPunti.push_back(Point(0.0,-2.0));
     NuoviVertici.push_back(Vertex(Point(0.0,-2.0), 11));

     for(unsigned int i=0; i<Decagono.PuntiPoligono.size(); i++)
         EXPECT_EQ(Decagono.PuntiPoligono[i] , NuoviPunti[i]);

     vector<Polygon> NuoviPoligoni;
     vector<Vertex> VerticiCostr = {NuoviVertici[0], NuoviVertici[1], NuoviVertici[11]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[2], NuoviVertici[3], NuoviVertici[8], NuoviVertici[1]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[4], NuoviVertici[5], NuoviVertici[10], NuoviVertici[3]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[6], NuoviVertici[7], NuoviVertici[8], NuoviVertici[3], NuoviVertici[10]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[9], NuoviVertici[11], NuoviVertici[1], NuoviVertici[8]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     for(unsigned int i=0; i<PoligoniTagliati.size(); i++)
         EXPECT_EQ(PoligoniTagliati[i], NuoviPoligoni[i]);

    }

    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestDecagonoB, TestCutPolygon)
  {
    vector<Point> PuntiDecagonoB;
    vector<unsigned int> Indici = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    vector<double> coord = {2.0, -2.0, 0.0, -1.0, 3.0, 1.0, 0.0, 2.0, 3.0, 2.0, 3.0, 3.0, -1.0, 3.0, -3.0, 1.0, 0.0, 0.0, -3.0, -2.0};
    Segment segmento = Segment(Point(-4.0, -4.0),Point(4.0, 4.0));
    for(unsigned int i=0; i<coord.size(); i+=2){
        PuntiDecagonoB.push_back(Point(coord[i], coord[i+1]));
    }

    vector<Vertex> VerticiDecagonoB;

    for(unsigned int i=0; i<Indici.size(); i++){
        VerticiDecagonoB.push_back(Vertex(PuntiDecagonoB[i], Indici[i]));
    }

    Polygon DecagonoB(VerticiDecagonoB);
    vector<Polygon> PoligoniTagliati;

    try
    {
     PoligoniTagliati=DecagonoB.CutPolygon(DecagonoB.PuntiPoligono, Indici, segmento);
     vector<Point> NuoviPunti = PuntiDecagonoB;
     vector<Vertex> NuoviVertici = VerticiDecagonoB;
     NuoviPunti.push_back(Point(1.5,1.5));
     NuoviVertici.push_back(Vertex(Point(1.5,1.5), 10));
     NuoviPunti.push_back(Point(2.0,2.0));
     NuoviVertici.push_back(Vertex(Point(2.0,2.0), 11));
     NuoviPunti.push_back(Point(-2.0,-2.0));
     NuoviVertici.push_back(Vertex(Point(-2.0,-2.0), 12));


     for(unsigned int i=0; i<DecagonoB.PuntiPoligono.size(); i++)
         EXPECT_EQ(DecagonoB.PuntiPoligono[i] , NuoviPunti[i]);

     vector<Polygon> NuoviPoligoni;
     vector<Vertex> VerticiCostr = {NuoviVertici[0], NuoviVertici[1], NuoviVertici[2], NuoviVertici[10], NuoviVertici[8], NuoviVertici[12]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[4], NuoviVertici[5], NuoviVertici[11]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[3], NuoviVertici[11], NuoviVertici[5], NuoviVertici[6], NuoviVertici[7], NuoviVertici[8], NuoviVertici[10]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[9], NuoviVertici[12], NuoviVertici[8]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     for(unsigned int i=0; i<PoligoniTagliati.size(); i++)
         EXPECT_EQ(PoligoniTagliati[i], NuoviPoligoni[i]);

    }

    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestRombo, TestCutPolygon)
  {
    vector<Point> PuntiRombo;
    vector<unsigned int> Indici = {0, 1, 2, 3};
    vector<double> coord = {2.0, 0.0, 0.0, 4.0, -2.0, 0.0, 0.0, -4.0};
    Segment segmento = Segment(Point(0.0, 0.0),Point(-10.0, 10.0));
    for(unsigned int i=0; i<coord.size(); i+=2){
        PuntiRombo.push_back(Point(coord[i], coord[i+1]));
    }

    vector<Vertex> VerticiRombo;

    for(unsigned int i=0; i<Indici.size(); i++){
        VerticiRombo.push_back(Vertex(PuntiRombo[i], Indici[i]));
    }

    Polygon Rombo(VerticiRombo);
    vector<Polygon> PoligoniTagliati;

    try
    {
     PoligoniTagliati=Rombo.CutPolygon(Rombo.PuntiPoligono, Indici, segmento);
     vector<Point> NuoviPunti = PuntiRombo;
     vector<Vertex> NuoviVertici = VerticiRombo;
     NuoviPunti.push_back(Point(-1.3333,1.3333));
     NuoviVertici.push_back(Vertex(Point(-1.3333,1.3333), 4));
     NuoviPunti.push_back(Point(1.3333,-1.3333));
     NuoviVertici.push_back(Vertex(Point(1.3333,-1.3333), 5));
     NuoviPunti.push_back(Point(0.0,0.0));
     NuoviVertici.push_back(Vertex(Point(0.0,0.0), 6));


     for(unsigned int i=0; i<Rombo.PuntiPoligono.size(); i++)
         EXPECT_EQ(Rombo.PuntiPoligono[i] , NuoviPunti[i]);

     vector<Polygon> NuoviPoligoni;
     vector<Vertex> VerticiCostr = {NuoviVertici[0], NuoviVertici[1], NuoviVertici[4], NuoviVertici[6], NuoviVertici[5]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     VerticiCostr = {NuoviVertici[2], NuoviVertici[3], NuoviVertici[5], NuoviVertici[6], NuoviVertici[4]};
     NuoviPoligoni.push_back(Polygon(VerticiCostr));
     VerticiCostr.clear();

     for(unsigned int i=0; i<PoligoniTagliati.size(); i++)
         EXPECT_EQ(PoligoniTagliati[i], NuoviPoligoni[i]);

    }

    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestConcEsagono, TestCreateMesh){
      vector<Point> PuntiConcavo;
      vector<unsigned int> Indici = {0, 1, 2, 3, 4, 5};
      vector<double> coord = {1.5, 1.0, 5.6, 1.5, 5.5, 4.8, 4.0, 6.2, 3.2, 4.2, 1.0, 4.0};
      for(unsigned int i=0; i<coord.size(); i+=2){
          PuntiConcavo.push_back(Point(coord[i], coord[i+1]));
      }
      vector<Vertex> VerticiConcavo;
      for(unsigned int i=0; i<Indici.size(); i++){
          VerticiConcavo.push_back(Vertex(PuntiConcavo[i], Indici[i]));
      }

      Polygon Concavo(VerticiConcavo);

      try{
      Polygon BB = Concavo.CreateBoundingBox();
      double l = BB.SegmentiPoligono[0].B.X - BB.SegmentiPoligono[0].A.X;
      double h = BB.SegmentiPoligono[1].B.Y - BB.SegmentiPoligono[1].A.Y;
      vector<Vertex> VerticiDominio;
      VerticiDominio.push_back(Vertex(Point(0,0), 45));
      VerticiDominio.push_back(Vertex(Point((4*l)+1, 0), 46));
      VerticiDominio.push_back(Vertex(Point((4*l)+1,(3*h)+1), 47));
      VerticiDominio.push_back(Vertex(Point(0,(3*h)+1), 48));
      Polygon Dominio = Polygon(VerticiDominio);
      Polygon::ReferenceElement RE = Concavo.CreateReferenceElement();
      vector<Polygon::ReferenceElement> vectorRef = Concavo.CreateMesh(RE, Dominio);
      double AreaMesh = 0;
      for(unsigned int i=0; i<vectorRef.size(); i++){
          AreaMesh += vectorRef[i].ComputeAreaRE();
      }

      double AreaDominio = ((4*l)+1) * ((3*h)+1);
      double toll = 1.0e-4;
      if(abs(AreaDominio - AreaMesh) < toll)
          AreaDominio = AreaMesh;

      EXPECT_EQ(AreaDominio, AreaMesh);
  }

      catch (const exception& exception)
      {
        FAIL();
      }

}

}

#endif // __TEST_EMPTYCLASS_H
